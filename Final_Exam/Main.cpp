#include <iostream>
#include <string>
using namespace std;

#pragma region Directions & Advice

/*
DIRECTIONS:
Complete as many of the tasks below as you can.
Nothing you write in main will be counted; use it as your scratch paper for testing things.

Here�s some advice:
- Read all tasks first. Immediately answer any that you think will be quick and easy.
- Complete at least 1, 3, 4, 5, 6, and either 9 or 10. Complete the others if you have time.
- Focus on getting the right answer. Only worry about commenting and the coding standard if you are instructed to, or if you have extra time.
- Do not consider your answer complete until you have tested it.
- Figuring out the questions is not part of the exam. If you don�t understand what you�re being asked to do, raise your hand.
- When given function specifications, you must follow them PRECISELY. For example, do not add side effects or return values that aren't requested.
- Remember that you can use the internet and any other resources you have, with the following exceptions:
  - Don�t talk to anyone.
  - Don�t copy exact solutions from the internet.
- Don�t forget to breathe, drink water, and take your time. Don�t panic.
- Use the + and - buttons to the left to expose and hide things as you work on them.
*/

#pragma endregion

#pragma region Task 1

/*
TASK 1
Write a function to play with randomizing float numbers:
- Parameters : none
- Side effects :
  - Allocate a float array with a size of 5 and assign each element as follows :
  - Generate 2 random numbers
  - Divide one by the other and assign it to the element as a float
  - Be sure to avoid rounding to whole numbers
  - Then print each element in reverse order
- Return : nothing
- Example:
  - examples of random numbers are 3/10, 13/11, 14/2
  - each above expression can and should be stored as a fractional number, i.e. .3333, 1.1818, 7
  - they would be printed backwards, so in this case 7, 1.1818, .3333
*/

// Put your solution to #1 here.

void RandomFloat()
{
	float arr[] { -1, -1, -1, -1, -1 };

	srand((unsigned int)time(NULL));  // init random seed

	for (int index = 0; index < 5; ++index)
	{
		int num1 = rand();
		int num2 = rand();

		arr[index] = (float)num1 / num2;
	}

	for (int index = 4; index >= 0; --index)
	{
		cout << arr[index] << endl;
	}
}

#pragma endregion

#pragma region Task 2

/*
TASK 2
Write a function called AskUserForCharacters() that meets these specifications:
- Purpose: to get the user to enter characters one at a time and count certain characters
- Required: NO GLOBAL VARIABLES USED
- Parameters: * (you decide what parameters are appropriate)
- Side effects:
  - Ask the user to enter a character
  - Count the number of times they enter the characters q, y, or p
  - The count of each character should be recorded in such a way to be accessible outside the function
- Return:
  - nothing
- Example usage:

void TaskTwoExample()
{
	// other code
	// AskUserForCharaters(*) is called here with appropriate arguments if any
	// the number of q keys can be printed here
	// the number of y keys can be printed here
	// the number of p keys can be printed here
}

*/

// Put your solution to #2 here.

void AskUserForCharacters(int& q, int& y, int& p)
{
	// Initialize before execution
	{
		q = 0;
		y = 0;
		p = 0;
	}

	bool exit = false;

	while (!exit)
	{
		cout << "Please enter a character: ";
		char input;
		cin >> input;

		switch (input)
		{
		case 'q': ++q; break;
		case 'y': ++y; break;
		case 'p': ++p; break;
		default:
			exit = true;
			break;
		}
	}
}

void Task2()
{
	int q, y, p;
	AskUserForCharacters(q, y, p);
	cout << "q: " << q << endl;
	cout << "y: " << y << endl;
	cout << "p: " << p << endl;
}

#pragma endregion

#pragma region Task 3

/*
TASK 3
Write a function called GenerateArray() that meets these specifications
- Purpose: generate an array of random numbers and return it.
- Parameters: two numbers n and m
- Side effects:
  - Check that n is greater than zero, and that n is less than or equal to m. If not, end the function immediately.
  - Generate an array of numbers with (a random size between n and m) + 1
  - Fill the array with random numbers between n and m
  - Fill the last element with the value zero
- Return: the generated array
- Example:
  - 3 and 6 are passed in
  - a number from the range [3,6] is pulled. Let's say it's 4.
  - an array with a size of 4 + 1 = 5 is generated
  - 4 more numbers from the range [3,6] are pulled and put int the array
  - 0 is assigned to the last element
  - the array is returned from the function.
*/


// Put your solution to #3 here.

int* GenerateArray(int n, int m)
{
	if (n < 0)
		return nullptr;

	if (n > m)
		return nullptr;

	srand((unsigned int)time(NULL));      // init random seed

	int size = (rand() % (m - n + 1) + n) + 1;  // create size

	int* arr = new int[size];             // generate array, ready to return at the end

	for (int index = 0; index < size - 1; ++index)
	{
		int number = rand() % (m - n + 1) + n;  // generate random number
		arr[index] = number;                    // storing random numbers
	}

	arr[size - 1] = 0;  // assign last element as 0

	return arr;         // return it
}

#pragma endregion

#pragma region Task 4

/*
TASK 4
Demonstrate the usage of the function you wrote in #3 by performing these steps in a new function:
- Write a loop that does the following 5 times:
  - Call GenerateArray(), passing 4 and 8 as arguments.
  - Use a loop to print all of the contents of the generated array; stop before printing the zero.
  - Perform any necessary cleanup of the output of GenerateArray().

*/

void TestTask3()
{
	// Put your solution to task 4 here.
	int* arr = GenerateArray(4, 8);

	for (int* p = arr; *p; ++p)
	{
		printf("Num is: %d\n", *p);
	}

	delete[] arr;
}

#pragma endregion

#pragma region Task 5

/*
TASK 5
Using best practices, define a data type called Player with the following members:
- the player�s username
- the player�s friend list (they may have up to 255 friends; each friend is another Player)
- a SetFriend function:
	- Inputs:
		- friend: another player
		- n: an integer
	- Side effects:
		- Stores the address of friend in the nth element of the friend list
	- Example:
		- player1.SetFriend(player2, 0)
		- player1 now stores the address of player2 at index 0 of its friend list

*/


// Put your solution to task 5 here.

struct Player
{
	const int kSize = 255;

	string m_username;
	Player** m_pFriends;

	Player(string name)
	{
		m_username = name;
		m_pFriends = new Player*[kSize];
	}

	~Player()
	{
		delete[] m_pFriends;
	}

	void SetFriend(Player* player, int n)
	{
		m_pFriends[n] = player;
	}
};

#pragma endregion

#pragma region Task 6

/*
TASK 6
Demonstrate you know how to work with objects by doing the following:
- allocate two Player objects:
	- player 1 should be named �Neo�
	- player 2 should be named �Trinity�
	- player 3 should be named �Morpheus�
	- Set Neo�s 0th friend to Trinity and 1st friend to Morpheus
	- Set Trinity�s 1st friend to Morpheus and 3rd friend to Neo
	- Set Morpheus� 253th friend to Trinity and 254th friend to Neo

*/

void TestTask5()
{
	// Put your solution to task 6 here.
	Player* player1 = new Player("Neo");
	Player* player2 = new Player("Trinity");
	Player* player3 = new Player("Morpheus");

	player1->SetFriend(player2, 0);
	player1->SetFriend(player3, 1);

	player2->SetFriend(player3, 1);
	player2->SetFriend(player1, 3);

	player3->SetFriend(player2, 253);
	player3->SetFriend(player1, 254);

	delete player1;
	delete player2;
	delete player3;
}

#pragma endregion

#pragma region Task 7

/*
TASK 7

Write a new data type called PremiumPlayer that has everything Player has but add these members:
- the player�s premium currency wallet (integer; the amount of money they have earned in the game)
- a ModifyWallet function that can be used to increase or decrease this player�s wallet by n
	- Examples: player.ModifyWallet(100) to gain $100, or player.ModifyWallet(-100) to lose $100
- a GiftCurrency function that can be used to gift money to another user on the player's friends list
	- Example: player.GiftCurrency(20, 0) will give $20 to the 0th friend on their friends list


*/


// Put your solution to task 7 here.

struct PremiumPlayer
{
	const int kSize = 255;

	string m_username;
	PremiumPlayer** m_pFriends;

	int currency;

	PremiumPlayer(string name)
	{
		m_username = name;
		m_pFriends = new PremiumPlayer * [kSize];
		currency = 0;
	}

	~PremiumPlayer()
	{
		delete[] m_pFriends;
	}

	void SetFriend(PremiumPlayer* player, int n)
	{
		m_pFriends[n] = player;
	}

	void ModifyWallet(int delta)
	{
		currency += delta;
	}

	void GiftCurrency(int delta, int index)
	{
		m_pFriends[index]->ModifyWallet(delta);
		ModifyWallet(-delta);
	}
};

#pragma endregion

#pragma region Task 8

/*
TASK 8

Replicate your answer to #6 except for these changes:
- Use PremiumPlayer instead of Player
- After setting up the PremiumPlayers as in #6:
	- give Morpheus $1000
	- have Morpheus give $200 to Neo using the GiftCurrency function.

*/

void TestTask7()
{
	// Put your solution to task 6 here.
	PremiumPlayer* player1 = new PremiumPlayer("Neo");
	PremiumPlayer* player2 = new PremiumPlayer("Trinity");
	PremiumPlayer* player3 = new PremiumPlayer("Morpheus");

	player1->SetFriend(player2, 0);
	player1->SetFriend(player3, 1);

	player2->SetFriend(player3, 1);
	player2->SetFriend(player1, 3);

	player3->SetFriend(player2, 253);
	player3->SetFriend(player1, 254);

	player3->ModifyWallet(1000);
	player3->GiftCurrency(200, 253);

	delete player1;
	delete player2;
	delete player3;
}

#pragma endregion

#pragma region Task 9

/*
TASK 9
This function is deliberately left un-commented, and has one or more errors. Do the following:
- Find out what the error or errors are.
	- Write a comment explaining all the methods or tools that you can use to test and locate the problem.
- Investigate what is causing the error or errors.
	- Write a comment explaining all of the methods or tools that you can use to investigate.
- Fix the error or errors
	- Write a comment explaining all of the methods or tools that you can use to fix the error, including alternative solutions if any.
If you see any other improvements you can make, feel free to do so, but leave an additional comment explaining what you did and why.
*/

////////////////////////////////////////////////////////////////////////////////////
// ToLower: Uses a mathematical property of the ASCII table to convert all capital
// letters to their lowercase counterparts. Other characters should not be affected.
//
// Example: "Hello, World!" becomes "hello, world!"
//
// Parameters: aString: any string.
// Returns: the modified string.
////////////////////////////////////////////////////////////////////////////////////
string ToLower(string aString)
{
	// The trick is that all uppercase letters are in the range between 65 ('A')
	// and 90 ('Z') and that you can add 32 ('a' - 'A') to any uppercase
	// letter to get its lowercase version. (e.g. 'Z' + 32 = 'z')

	constexpr char kFirstCapitalAsciiChar = 'A';                  // Establish first ascii character for range.
	constexpr char kLastCapitalAsciiChar = 'Z';                   // Establish last ascii character for range.
	constexpr int kDiffBetweenCapitalAndLowercase = ('a' - 'A');  // Find differences

	// Loop through the string.
	for (size_t i = 0; i < aString.size(); ++i)
	{
		char currentChar = aString.at(i);  // this is on stack

		// Check ascii range.
		if (currentChar >= kFirstCapitalAsciiChar
			&& currentChar <= kLastCapitalAsciiChar)
		{
			aString[i] += kDiffBetweenCapitalAndLowercase;  // Assign it to the memory instead.
		}
	}

	return aString;
}

// Explain how to test and find the error here.

/*
* Try to call the function and see what's wrong.
* 
* For example:
* 
*   ```c++
*   cout << ToLower("Hello World") << endl;
*   ```
* 
* And we expect the output to be "hello world", but it turns out the result remains the same.
*/

// Explain how to investigate the cause of the error here.

/*
* By looking at the code, we see:
* 
*   ```c++
*   currentChar += kDiffBetweenCapitalAndLowercase;
*   ```
* 
* It means change the value on the stack (for loop), and not in the previous stack frame.
* Therefore, the value does not get updated.
*/

// Explain how to fix the error including alternatives here.

/*
* It's easy to fix it by pointing to the correct stacking variable `aString`.
* 
*   ```diff
*   - currentChar += kDiffBetweenCapitalAndLowercase;
*   + aString[i] += kDiffBetweenCapitalAndLowercase;
*   ```
*/

#pragma endregion

#pragma region Task 10

/*
TASK 10
This function is deliberately left un-commented, and has one or more errors. Do the following:
- Find out what the error or errors are.
	- Write a comment explaining all the methods or tools that you can use to test and locate the problem.
- Investigate what is causing the error or errors.
	- Write a comment explaining all of the methods or tools that you can use to investigate.
- Fix the error or errors
	- Write a comment explaining all of the methods or tools that you can use to fix the error, including alternative solutions if any.
If you see any other improvements you can make, feel free to do so, but leave an additional comment explaining what you did and why.
*/

////////////////////////////////////////////////////////////////////////////////////
// EnterName: Prompts the user to enter their name. Gives them a chance to start
// over if they make a mistake.
// 
// Returns: a pointer to the entered name.
////////////////////////////////////////////////////////////////////////////////////
char* EnterName()
{
	constexpr int kYesResponseCount = 2;         // Size of the response count.
	constexpr char kYesResponses[]{ 'y', 'Y' };  // A list of character to accept input

	cout << "Enter the length of your name: ";
	int size;
	cin >> size;

	char* pName = new char[size];
	cout << "Now enter your name (no spaces, please): ";
	cin >> pName;

	cout << "You entered: " << pName << ". Is this correct? (y for yes) ";
	char yesOrNo;
	cin >> yesOrNo;

	// Check if input 'y' or 'Y'.
	for (int i = 0; i < kYesResponseCount; ++i)
	{
		if (yesOrNo == kYesResponses[i])
		{
			// If correct, return the inputted name.
			return pName;
		}
	}

	// Else we try again!
	return EnterName();
}

// Explain how to test and find the error here.

/*
* 1. Put the function `EnterName` into main function.
* 2. Start the debugger.
* 3. See what goes wrong.
*/

// Explain how to investigate the cause of the error here.

/*
* I discovered 2 errors,
*   1. No defense programming to check if the user enters the right value
*   2. Logic error, the program doesn't check the size and the actual inputted name's size.
*      But I guess this is the design decision.
*/

// Explain how to fix the error including alternatives here.

/*
* Defense programming is a hard task depends on what you've done. The best way to resolve
* this is to add check at the end of every input.
*/

#pragma endregion

int main()
{
	//RandomFloat();
	//Task2();
	//TestTask3();
	//TestTask5();
	TestTask7();
	//cout << ToLower("Hello WEorld") << endl;
	//EnterName();

	// Use this for testing only.
	system("pause");
	return 0;
}









