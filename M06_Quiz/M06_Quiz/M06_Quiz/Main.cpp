/**
 * $File: Main.cpp $
 * $Date: 2023-03-16 15:05:05 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */

#include <iostream>

/**
 * Program Entry
 * @return Exit code
 */
int main()
{
    const int divider = 3;

    for (int count = 1; count < 100; ++count)
    {
        if (count % divider == 0)
            std::cout << count << std::endl;
    }

    return 0;
}
