---
title: README.md
date: 2023-03-16 14:57:40
description: Anything regarding the Module 6 Quiz
---


#### 1. Consider the following code snippet.

```c++
string name;  // decalre a variable name `string`

cin >> name;  // halt program to receive input, input buffer will be stored in `string` memory
```

C++ is a strongly typed language, so every input will convert into a string. It
could be an hassle for certain situation.

#### 2. Please complete the table below by entering the correct data type for each value.

| Expression | Data Type |
|------------|-----------|
| `"3"`      | string    |
| `3`        | integer   |
| `3.f`      | float     |
| `'3'`      | char      |
| `3.0`      | double    |


#### 3. Write a program that prints all multiples of 3 between 1 and 100.

See project `M06_Quiz`.
